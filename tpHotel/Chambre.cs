﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tpHotel
{
    class Chambre
    {
        private int Num;
        private int etage;
        private string description;
        private int idHotel;
        public Chambre(int UnNum , int UnEtage, string UneDescription, int UnId)
        {
            Num = UnNum;
            etage = UnEtage;
            description = UneDescription;
            idHotel = UnId;

        }

        public int Num1 { get => Num; set => Num = value; }
        public int Etage { get => etage; set => etage = value; }
        public string Description { get => description; set => description = value; }
        public int IdHotel { get => idHotel; set => idHotel = value; }
    }
}
