﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tpHotel
{
    class Hotel
    {
        private int id;
        private string nom;
        private string adresse;
        private string ville;
        public Hotel(int UnId, string UnNom, string UneAdresse, string UneVille)
        {
            id = UnId;
            nom = UnNom;
            adresse = UneAdresse;
            ville = UneVille;
        }

        public int Id { get => id; set => id = value; }
        public string Nom { get => nom; set => nom = value; }
        public string Adresse { get => adresse; set => adresse = value; }
        public string Ville { get => ville; set => ville = value; }
    }
   
}
