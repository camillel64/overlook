﻿namespace tpHotel
{
    partial class frmVoirChambre
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstChambres = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.lstChambres)).BeginInit();
            this.SuspendLayout();
            // 
            // lstChambres
            // 
            this.lstChambres.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.lstChambres.Location = new System.Drawing.Point(98, 36);
            this.lstChambres.Name = "lstChambres";
            this.lstChambres.Size = new System.Drawing.Size(240, 150);
            this.lstChambres.TabIndex = 0;
            this.lstChambres.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.lstChambres_CellContentClick);
            // 
            // frmVoirChambre
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(406, 263);
            this.Controls.Add(this.lstChambres);
            this.Name = "frmVoirChambre";
            this.Text = "frmVoirChambre";
            this.Load += new System.EventHandler(this.frmVoirChambre_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lstChambres)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView lstChambres;
    }
}