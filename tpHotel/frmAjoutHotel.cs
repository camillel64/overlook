﻿using System;
using System.Windows.Forms;

namespace tpHotel
{
    public partial class frmAjoutHotel : Form
    {
        public frmAjoutHotel()
        {
            InitializeComponent();
        }

        private void btnFermer_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            this.raz();
        }

        private void btnEnregistrer_Click(object sender, EventArgs e)
        {
            Persistance.ajouteHotel(txtNom.Text, txtAdresse.Text, txtVille.Text);
            MessageBox.Show("Le message a bien été envoyé.", "Information");

        }

        private void raz()
        {
            this.txtNom.Text = "";
            this.txtAdresse.Text = "";
            this.txtVille.Text = "";
            this.txtNom.Focus();
        }
    }
}
