﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tpHotel
{
    public partial class frmAjoutChambre : Form
    {
        public frmAjoutChambre()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnEnregistrer_Click(object sender, EventArgs e)
        {
            //utiliser get hotel getLesHotels(); pour recup la liste des hotels, parcourir la liste avec foreach, récupérer de nom de chaque hotel dans la liste grace à listeHotel.items.add(...)
            Persistance.ajouteChambre((int)lstEtage.Value, txtdesc.Text, (int)lstHotel.SelectedItem);
            MessageBox.Show("Le message a bien été envoyé.", "Information");
        }

        private void raz()
        {
            this.lstEtage.Text = "";
            this.txtdesc.Text = "";
            this.lstHotel.Text = "";
            this.lstEtage.Focus();
        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            this.raz();
        }

        private void btnFermer_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmAjoutChambre_Load(object sender, EventArgs e)
        {

            foreach (Hotel element in Persistance.getLesHotels())
            {
                lstHotel.Items.Add(element.Id);
            }
            /*utiliser get hotel getLesHotels(); pour recup la liste des hotels, 
        parcourir la liste avec foreach, récupérer de nom de chaque hotel dans
            la liste grace à listeHotel.items.add(...)*/
        }

        private void lstHotel_SelectedIndexChanged(object sender, EventArgs e)
        {
 
        }

        private void lstHotel_ValueChanged(object sender, EventArgs e)
        {

        }

        private void lstHotel_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }
    }
}
